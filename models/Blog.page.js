// https://playwright.dev/docs/pom
const BasePage = require('./Base.page');
class BlogPage extends BasePage {
    constructor(page){
        super(page);

        // selectors
        this.menuBtn =  '.inline-flex.p-2';
        this.loginLnk = 'a[href="/account/"]';
        

    }
    /**
     * Method to navigate to Blog page using parent's method
     */
    async navigate(){
       await super.navigate('blog'); 
    }
    /**
     * Method to login using username and password
     * @param {string} username 
     * @param {string} password 
     */
    async goLogin(){
        const menu = await this.page.$(this.menuBtn);
        if (menu) {
            await this.page.click(this.menuBtn);
        }
        await this.page.click(this.loginLnk);       
    }

}
module.exports = BlogPage;