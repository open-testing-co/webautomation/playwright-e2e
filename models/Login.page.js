// https://playwright.dev/docs/pom
const BasePage = require('./Base.page');
class LoginPage extends BasePage {
    constructor(page){
        super(page);

        // selectors
        this.userNameTxt =  '[name="login"]';
        this.passwordTxt =  '[name="password"]';
        this.loginBtn = '#login-btn';
        this.userErrorTxt = '#mat-error-2';
        this.passErrorTxt = '#mat-error-1';
    }
    /**
     * Method to navigate to Login page using parent's method
     */
    async navigate(){
       await super.navigate('account/auth/login/start'); 
    }
    /**
     * Method to login using username and password
     * @param {string} username 
     * @param {string} password 
     */
    async login(username, password){
        await this.page.fill(this.userNameTxt,username);
        await this.page.fill(this.passwordTxt,password);
        await this.page.click(this.loginBtn);
    }

    /**
     * Method to retrieve the username error
     * @returns {string} username error
     */
    async getUserError(){
        let error = await this.page.$(this.userErrorTxt);
        return await error.innerText();
    }

    /**
     * Method to retrieve the password error
     * @returns {string} password error
     */
    async getPassError(){
        let error = await this.page.$(this.passErrorTxt);
        return await error.innerText();
    }
}
module.exports = LoginPage;