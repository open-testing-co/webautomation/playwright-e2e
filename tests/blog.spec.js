const { test, expect } = require('@playwright/test');
const BlogPage = require('../models/Blog.page');

test('Title', async ({ page }) => {
  let blogPage = new BlogPage(page);
  await blogPage.navigate();
  await expect(page).toHaveTitle("Aspiring Automation | ElectroNeek");
});

test('Login', async ({ page }) => {
    let blogPage = new BlogPage(page);
    await blogPage.navigate();
    await blogPage.goLogin();
    await expect(page).toHaveTitle("Authorization");
  });
  
