// @ts-check
const { test, expect } = require('@playwright/test');
const LoginPage = require('../models/Login.page');

test('E-mail invalid and pass required', async ({ page }) => {
  let loginPage = new LoginPage(page);
  await loginPage.navigate();
  await loginPage.login('username','');
  expect(await loginPage.getUserError() === "E-mail is not valid");
  expect(await loginPage.getPassError() === "Password is required");
});

test('E-mail invalid', async ({ page }) => {
  let loginPage = new LoginPage(page);
  await loginPage.navigate();
  await loginPage.login('username','password');
  expect(await loginPage.getUserError() === "E-mail is not valid");
});

test('Password required', async ({ page }) => {
  let loginPage = new LoginPage(page);
  await loginPage.navigate();
  await loginPage.login('username','');
  expect(await loginPage.getPassError() === "Password is required");
});